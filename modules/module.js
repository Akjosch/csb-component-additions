import ActorComponent from "./components/ActorComponent.js";
import "/systems/custom-system-builder/module/sheets/components/ComponentFactory.js";

Hooks.once('customSystemBuilderInit', () => {
    componentFactory.addComponentType('actor', ActorComponent);
});
