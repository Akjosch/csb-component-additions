import Component from '/systems/custom-system-builder/module/sheets/components/Component.js';
import TemplateSystem from '/systems/custom-system-builder/module/documents/templateSystem.js';
import Container from '/systems/custom-system-builder/module/sheets/components/Container.js';
import { CustomActor } from '/systems/custom-system-builder/module/documents/actor.js';

const actor = (a) =>  /** @type {Game} */ (game).actors?.get(a);
const t = (txt) => /** @type {Game} */ (game).i18n?.localize(txt);

class ActorComponent extends Component {
	/**
	 * Actor ID
	 * @type {string|null}
	 * @private
	 */
	_actor;

	/**
	 * Actor type
	 * @type {string}
	 * @private
	 */
	_objectKey;

	/**
	 * Actor type
	 * @type {string|null}
	 * @private
	 */
	_allowedTemplate;

	/**
	 * Actor token width
	 * @type {string|null}
	 * @private
	 */
	_width;

	/**
	 * Actor token height
	 * @type {string|null}
	 * @private
	 */
	_height;

	_element;
	/**
	 * @type {Object}
	 * @private
	 */
	_bindings;

	constructor({
		key,
		tooltip = null,
		templateAddress,
		cssClass = null,
		role = 0,
		permission = 0,
		visibilityFormula = null,
		parent = null,
		objectKey = null,
		allowedTemplate = null,
		width = "auto",
		height = "auto"
	}) {
		super({
			key: key,
			tooltip: tooltip,
			templateAddress: templateAddress,
			cssClass: cssClass,
			role: role,
			permission: permission,
			visibilityFormula: visibilityFormula,
			parent: parent
		});

		this._objectKey = objectKey;
		this._allowedTemplate = allowedTemplate;
		this._width = width;
		this._height = height;
		this._actor = null;
		this._element = null;
		this._bindings = {};
	}

	/**
	 * Component property key
	 * @override
	 * @return {string}
	 */
	get propertyKey() {
		return this.key;
	}

	get entity() {
		return actor(this._actor);
	}

	get entityImage() {
		return actor(this._actor)?.img ?? this.templateImage;
	}

	get objectKey() {
		return this._objectKey;
	}

	get template() {
		return actor(this.entity?.system.template);
	}

	get allowedTemplate() {
		return this._allowedTemplate;
	}

	get templateImage() {
		return actor(this._allowedTemplate)?.img;
	}

	get templateName() {
		return actor(this._allowedTemplate)?.name;
	}

	get width() {
		return this._width;
	}

	get height() {
		return this._height;
	}

	_bind(key, proc) {
		Object.defineProperty(this._bindings, key, { set: proc });
	}

	/**
	 * Renders component
	 * @override
	 * @param {TemplateSystem} entity Rendered entity (actor or item)
	 * @param {boolean} [isEditable=true] Is the component editable by the current user ?
	 * @param {Object} [options={}] Additional options usable by the final Component
	 * @param {Object} [options.customProps = {}] Props to merge to the entity props for formula computing
	 * @param {Object} [options.linkedEntity = null] Linked entity, passed to the formula computing
	 * @param {Object} [options.dynamicRowRef = null] Dynamic Table row reference, passed to the formula computing
	 * @return {Promise<JQuery<HTMLElement>>} The jQuery element holding the component
	 */
	async _getElement(entity, isEditable = true, options = {}) {
		if (this._element) {
			this._bindings.editable = isEditable;
			this._bindings.actor = (entity.system.props ?? {})[this.key];
			this._bindings.width = this.width;
			this._bindings.height = this.height;
			return this._element;
		}
		this._element = await super._getElement(entity, isEditable, options);

		let wrapper = this._element.get(0);
		wrapper.classList.add('csb-additional-component');
		let holder = document.createElement('input');
		holder.type = 'hidden';
		if (!entity.isTemplate) {
			holder.name = 'system.props.' + this.key;
		}
		wrapper.appendChild(holder);
		let img = document.createElement('img');
		img.classList.add('actor-token');
		if (entity.isTemplate) {
			img.classList.add('custom-system-editable-component');
			img.addEventListener('click', () => {
				this.editComponent(entity);
			});
		} else {
			img.classList.add('custom-system-droppable-container');
			img.addEventListener('dragenter', (ev) => {
				ev.stopPropagation();
				ev.preventDefault();
				img.classList.add('custom-system-template-dragged-over');
			});
			img.addEventListener('dragover', (ev) => {
				ev.stopPropagation();
				ev.preventDefault();
			});
			img.addEventListener('dragleave', (ev) => {
				img.classList.remove('custom-system-template-dragged-over');
			});
			img.addEventListener('drop', async (ev) => {
				ev.stopPropagation();
				ev.preventDefault();
				img.classList.remove('custom-system-template-dragged-over');
				let dropData = JSON.parse(ev.dataTransfer.getData('text/plain'));
				if (dropData) {
					// @ts-ignore
					let actor = await CustomActor.fromDropData(dropData);
					if (actor && actor.type === 'character' && (!this._allowedTemplate || this._allowedTemplate === actor.system.template)) {
						let oldValue = holder.value;
						this._bindings.actor = actor.id;
						if (oldValue !== holder.value) {
							holder.dispatchEvent(new Event('change', { bubbles: true }));
						}
					}
				}
			});
		}
		wrapper.appendChild(img);
		/* bindings */
		this._bind('editable', (val) => holder.disabled == val ? '' : 'disabled');
		this._bind('actor', (val) => {
			if (!entity.isTemplate) {
				entity.system.props[this.key] = val;
				holder.value = val ?? '';
				this._actor = val;
				entity.system.props[this.objectKey] = this.entity?.toJSON();
				img.src = this.entityImage;
			} else {
				holder.value = '';
				this._actor = null;
				img.src = this.templateImage;
			}
		});
		this._bind('width', (val) => img.style.width = val)
		this._bind('height', (val) => img.style.height = val)
		this._bindings.editable = isEditable;
		this._bindings.actor = (entity.system.props ?? {})[this.key];
		this._bindings.width = this.width;
		this._bindings.height = this.height;

		return this._element;
	}

	/**
	 * Returns serialized component
	 * @override
	 * @return {Object}
	 */
	toJSON() {
		let jsonObj = super.toJSON();
		return {
			...jsonObj,
			objectKey: this.objectKey,
			allowedTemplate: this.allowedTemplate,
			width: this._width,
			height: this._height,
			type: 'actor'
		};
	}

	/**
	 * Creates ActorComponent from JSON description
	 * @override
	 * @param {Object} json
	 * @param {string} templateAddress
	 * @param {Container|null} parent
	 * @return {ActorComponent}
	 */
	static fromJSON(json, templateAddress, parent = null) {
		return new ActorComponent({
			key: json.key,
			tooltip: json.tooltip,
			templateAddress: templateAddress,
			objectKey: json.objectKey,
			allowedTemplate: json.allowedTemplate,
			width: json.width,
			height: json.height,
			cssClass: json.cssClass,
			role: json.role,
			permission: json.permission,
			visibilityFormula: json.visibilityFormula,
			parent: parent
		});
	}

	/**
	 * Gets pretty name for this component's type
	 * @return {string} The pretty name
	 * @throws {Error} If not implemented
	 */
	static getPrettyName() {
		return t('csb-component-additions.Actor');
	}

	/**
	 * Get configuration form for component creation / edition
	 * @param {Object} existingComponent Basic description of the existing component to pre-fill the form
	 * @param {TemplateSystem} entity The entity holding the existing component
	 * @return {Promise<JQuery<HTMLElement>>} The jQuery element holding the component
	 * @throws {Error} If not implemented
	 */
	static async getConfigForm(existingComponent, entity) {
		let mainElt = $('<div class="csb-additional-component"></div>');
		/* despite what the documentation says, existingComponent can be null/undefined */
		existingComponent = existingComponent || {};
		existingComponent.id = entity?.entity?.id;
		existingComponent.templateName = actor(existingComponent.allowedTemplate)?.name;
		mainElt.append(
			await renderTemplate(
				'modules/csb-component-additions/templates/_template/actor-component.html',
				existingComponent
			)
		);
		return mainElt;
	}

	/**
	 * Extracts configuration from submitted HTML form
	 * @abstract
	 * @param {JQuery<HTMLElement>} html The submitted form
	 * @return {Object} The basic representation of the component
	 * @throws {Error} If configuration is not correct
	 */
	static extractConfig(html) {
		let fieldData = super.extractConfig(html);
		if (!fieldData.key) {
			throw new Error(t('csb-component-additions.error.keyRequired'));
		}
		fieldData.objectKey = html.find('#actorObjectKey').val();
		if (!fieldData.objectKey) {
			throw new Error(t('csb-component-additions.error.objectKeyRequired'));
		}
		if (!fieldData.objectKey.match(/^[a-zA-Z0-9_]+$/)) {
			throw new Error(t('csb-component-additions.error.objectKeyFormat'));
		}
		if (fieldData.key === fieldData.objectKey) {
			throw new Error(t('csb-component-additions.error.keysSame'));
		}
		fieldData.allowedTemplate = html.find('#actorAllowedTemplate').val();
		fieldData.width = html.find('#actorWidth').val();
		fieldData.height = html.find('#actorHeight').val();
		return fieldData;
	}
}


export default ActorComponent;
